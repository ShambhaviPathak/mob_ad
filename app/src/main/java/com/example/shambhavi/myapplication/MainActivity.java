package com.example.shambhavi.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shambhavi.geofencinglibrary.MobmerryAd;

public class MainActivity extends AppCompatActivity {


    public static final int REQUEST_GPS_PERMISSION = 226;
    TextView textView;
    String url="https://www.google.co.in/?gfe_rd=cr&ei=JfBXV9DCAo2AogPe3qOoBg";
    private String DEBUG_TAG="Berry_example";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView=(TextView) findViewById(R.id.textview);

        if (((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED))) {

            MyApplication application=new MyApplication();
            application.getMobmerryAdInstance(this);
            //MobmerryAd.setConfigURL("");

        }else{
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_GPS_PERMISSION);
        }
/*
        if (!((ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
         == PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(MainActivity.this,
          Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED))) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_GPS_PERMISSION);
        }*/





      }

    @Override
    protected void onResume() {
        super.onResume();

        MobmerryAd.startBerryTimerService(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MobmerryAd.stopBerrryService(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_GPS_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   /* MyApplication application=new MyApplication();
                    application.getMobmerryAdInstance(this);*/
                } else {
                    Toast.makeText(MainActivity.this,"Permission not given",Toast.LENGTH_LONG).show();

                }
                MyApplication application=new MyApplication();
                application.getMobmerryAdInstance(this);
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


}
