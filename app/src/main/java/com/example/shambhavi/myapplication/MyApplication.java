package com.example.shambhavi.myapplication;

import android.app.Application;
import android.content.Context;

import com.example.shambhavi.geofencinglibrary.MobmerryAd;

/**
 * Created by akash on 31/5/16.
 */
public class MyApplication extends Application {

    private MobmerryAd mMobmerryAd;

    /**
     * Gets the default instance of MobmerryAd.
     * @return
     */
    synchronized public MobmerryAd getMobmerryAdInstance(Context context) {
        if (mMobmerryAd == null) {
            mMobmerryAd = MobmerryAd.getInstance(context);
        }
        return mMobmerryAd;
    }
}
