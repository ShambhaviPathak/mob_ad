package com.example.shambhavi.mylibrary;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class LibActivity extends AppCompatActivity {

   private WebView mywebView;
    private ImageView myImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lib_activity);
        startWebView("https://pizzaonline.dominos.co.in/menu/D/BHUBANESWAR/PATIA/66421");
        mywebView.setWebViewClient(new WebViewClient());
    }

    private void startWebView(String url) {
            myImageView=(ImageView) findViewById(R.id.image);
            mywebView=(WebView) findViewById(R.id.webview);
            WebSettings myWebSettings=mywebView.getSettings();
        myWebSettings.setJavaScriptEnabled(true);
        mywebView.loadUrl(url);
        mywebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
      myImageView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              finish();

          }
      });

    }





}