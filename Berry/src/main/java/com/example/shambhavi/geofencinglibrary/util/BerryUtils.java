package com.example.shambhavi.geofencinglibrary.util;

import android.Manifest;
import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by akash on 2/6/16.
 */
public  class BerryUtils {


    /**
     * This method checks if user has given Location Permission or not.
     * @param context Context from the calling activity
     * @return true if user has given the location permission,else return false.
     */
    public static boolean isLocationPermissionGiven(Context context) {

        PackageManager pm = context.getPackageManager();
        return pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, context.getPackageName())
                == PackageManager.PERMISSION_GRANTED;
    }


    /**
         * This method checks if phone is marshmallow or not.
         * @param context Context from the calling activity
         * @return true if phone is Marshmallow, else false
         */

    public static boolean isMarshmallow(Context context) {

        return Build.VERSION.SDK_INT >= 23;
    }

    /**
     * This method checks if GPS is ON/OFF
     *
     * @param context Context from the calling activity
     * @return true if GPS is ON, else false
     */
    public static boolean isGpsON(Context context) {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        //start Geofencing.
//handle the  Geofencing receive
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    /**
     * This method checks if Bluetooth is ON/OFF
     *
     * @param context Context from the calling activity
     * @return true if Bluetooth is ON, else false
     */
    public static boolean isBluetoothON(Context context) {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        // Bluetooth is not enable :)
        return !mBluetoothAdapter.isEnabled();
        }

    public static boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        try {
            ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                    if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        for (String activeProcess : processInfo.pkgList) {
                            if (activeProcess.equals(context.getPackageName())) {
                                isInBackground = false;
                            }
                        }
                    }
                }
            } else {
                List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                if (componentInfo.getPackageName().equals(context.getPackageName())) {
                    isInBackground = false;
                }
            }
        } catch (Exception e) {
            isInBackground = true;
        }


        return isInBackground;
    }


    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String downloadHTMLFromUrl(String urlToDownload) throws IOException {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        String result = "";
        try {
            url = new URL(urlToDownload);
            is = url.openStream(); // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                result = result + line;
            }

        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
// nothing to see here
            }
        }
        return result;

    }





}