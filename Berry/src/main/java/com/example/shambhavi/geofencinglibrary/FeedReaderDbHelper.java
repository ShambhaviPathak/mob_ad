package com.example.shambhavi.geofencinglibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by shambhavi on 15/6/16.
 */
public class FeedReaderDbHelper extends SQLiteOpenHelper implements BaseColumns  {

    public static final String TABLE_NAME = "entry";
    public static final String COLUMN_NAME_HTML_DATA = "HTML_DATA";
    public static final String TYPE="type";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HTMLReader.db";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" + _ID + " integer primary key autoincrement , " +
                 TYPE + " TEXT , " + COLUMN_NAME_HTML_DATA + " TEXT" +" )";
    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;
    public FeedReaderDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }



    // reading the data from the database
    public String getYourData(String Type) {



        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE " + TYPE +" = '"+ Type +"'";
        SQLiteDatabase db  = this.getReadableDatabase();
        Cursor cursor      = db.rawQuery(selectQuery, null);
        String data=null;

        if (cursor.moveToFirst()) {
            do {
                data = cursor.getString(cursor.getColumnIndex(COLUMN_NAME_HTML_DATA));
                // get the data into array, or class variable
            } while (cursor.moveToNext());
        }
        cursor.close();
        return data;
    }


    public String insertingDataIntoDatabase(String result,String Type){

        SQLiteDatabase db=getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(FeedReaderDbHelper.COLUMN_NAME_HTML_DATA, result);
        contentValues.put(FeedReaderDbHelper.TYPE,Type);
        db.insert(FeedReaderDbHelper.TABLE_NAME, "", contentValues);
        db.close();
        String htmlData = getYourData(Type);
        if (htmlData == null) {
            htmlData = "";
        }
       return htmlData;

    }



}
