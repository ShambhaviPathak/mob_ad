package com.example.shambhavi.geofencinglibrary.model;

import android.content.Context;

import com.android.volley.Request;
import com.example.shambhavi.geofencinglibrary.MobmerryAd;
import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;
import com.example.shambhavi.geofencinglibrary.controller.BaseInterface;
import com.example.shambhavi.geofencinglibrary.network.BaseNetwork;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by akash on 31/5/16.
 */
public class BerryAdModel extends BaseModel {

    @SerializedName("latitude")
    @Expose
    private String mLatitude;
    @SerializedName("longitude")
    @Expose
    private String mLongitude;
    @SerializedName("range")
    @Expose
    private String mRange;
    @SerializedName("title")
    @Expose
    private String mTitle;
    @SerializedName("description")
    @Expose
    private String mDescription;
    @SerializedName("adurl")
    @Expose
    private String mAdurl;
    @SerializedName("image_url")
    @Expose
    private String mImageurl;
    @SerializedName("loc_id")
    @Expose
    private String mId;
    @SerializedName("created")
    @Expose
    private String mCreated;
    @SerializedName("modified")
    @Expose
    private String mModified;

    /**
     * Constructor for the base model
     *
     * @param context       the application mContext
     * @param baseInterface it is the instance of baseinterface,
     */
    public BerryAdModel(Context context, BaseInterface baseInterface) {
        super(context, baseInterface);
    }

    public void getConfig() {
        BaseNetwork baseNetwork = new BaseNetwork(mContext, this);
        baseNetwork.getJSONObjectForRequest(Request.Method.GET, BerryConstant.CONFIG_URL, null, BerryConstant.CONFIG_REQUEST);
    }

    public void getGeofences() {
        BaseNetwork baseNetwork = new BaseNetwork(mContext, this);
        baseNetwork.getJSONArrayForRequest(Request.Method.GET, BerryConstant.LOCATION_URL, null, BerryConstant.LOCATION_REQEST);

    }

    @Override
    public void parseAndNotifyResponse(JSONObject response, int requestType) throws JSONException {
        try {
            Gson gson = new GsonBuilder().create();
            switch (requestType) {

                case BerryConstant.LOCATION_REQEST:
                    ArrayList<BerryAdModel> geofences = new ArrayList<>();

                    JSONArray geofenceArray = null;

                    try {
                        geofenceArray = response.getJSONArray(BerryConstant.GEOFENCES);

                        geofences = gson.fromJson(geofenceArray.toString(),
                                new TypeToken<List<BerryAdModel>>() {
                                }.getType());


                        //Pass the geofence to the calling class.
                        mBaseInterface.handleNetworkCall(geofences, requestType);



                    } catch (JSONException e) {
                        mBaseInterface.handleNetworkCall("Error parsing reject response", requestType);
                        geofences = new ArrayList<>();
                    }

                    break;

                case BerryConstant.CONFIG_REQUEST:
                    try {
                        AdurlsModel adUrlModel = gson.fromJson(response.toString(), AdurlsModel.class);
                        MobmerryAd.getInstance(mContext).setmGenericAdsUrl(adUrlModel.getmGenericads());
                        MobmerryAd.getInstance(mContext).setmGeofenceAdsUrl(adUrlModel.getmGeofenceads());
                        MobmerryAd.getInstance(mContext).setmBluetoothUrl(adUrlModel.getmBluetoothUrl());
                        mBaseInterface.handleNetworkCall(true, requestType);
                    } catch (Exception e) {
                        mBaseInterface.handleNetworkCall("Error in loading config", requestType);
                        e.printStackTrace();
                    }
                    break;
            }

        } catch (Exception e) {
            // Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public HashMap<String, Object> getRequestBody(int requestType) {
        return null;
    }


    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmRange() {
        return mRange;
    }

    public void setmRange(String mRange) {
        this.mRange = mRange;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmAdurl() {
        return mAdurl;
    }

    public void setmAdurl(String mAdurl) {
        this.mAdurl = mAdurl;
    }

    public String getmImageurl() {
        return mImageurl;
    }

    public void setmImageurl(String mImageurl) {
        this.mImageurl = mImageurl;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmCreated() {
        return mCreated;
    }

    public void setmCreated(String mCreated) {
        this.mCreated = mCreated;
    }

    public String getmModified() {
        return mModified;
    }

    public void setmModified(String mModified) {
        this.mModified = mModified;
    }
}
