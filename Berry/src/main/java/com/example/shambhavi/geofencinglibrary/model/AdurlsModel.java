package com.example.shambhavi.geofencinglibrary.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by akash on 31/5/16.
 */
public class AdurlsModel {

    @SerializedName("generic_url")
    @Expose
    private String mGenericads;
    @SerializedName("geofence_url")
    @Expose
    private String mGeofenceads;
    @SerializedName("bluetooth_url")
    @Expose
    private String mBluetoothUrl;
    @SerializedName("created")
    @Expose
    private String mCreated;
    @SerializedName("modified")
    @Expose
    private String mModified;

    public String getmGenericads() {
        return mGenericads;
    }

    public void setmGenericads(String mGenericads) {
        this.mGenericads = mGenericads;
    }

    public String getmGeofenceads() {
        return mGeofenceads;
    }

    public void setmGeofenceads(String mGeofenceads) {
        this.mGeofenceads = mGeofenceads;
    }

    public String getmBluetoothUrl() {
        return mBluetoothUrl;
    }

    public void setmBluetoothUrl(String mBluetoothUrl) {
        this.mBluetoothUrl = mBluetoothUrl;
    }

    public String getmCreated() {
        return mCreated;
    }

    public void setmCreated(String mCreated) {
        this.mCreated = mCreated;
    }

    public String getmModified() {
        return mModified;
    }

    public void setmModified(String mModified) {
        this.mModified = mModified;
    }
}
