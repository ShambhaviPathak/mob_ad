package com.example.shambhavi.geofencinglibrary;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;

public class LibActivity extends AppCompatActivity {

    WebViewClient wvc = new WebViewClient();
    private WebView mywebView;
    private ImageView myImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lib_activity);
        FeedReaderDbHelper dbHelper = new FeedReaderDbHelper(this);

        myImageView = (ImageView) findViewById(R.id.image);
        mywebView = (WebView) findViewById(R.id.webview);
        WebSettings myWebSettings = mywebView.getSettings();
        myWebSettings.setJavaScriptEnabled(true);
        mywebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mywebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        String htmlData = getIntent().getStringExtra(BerryConstant.HTML_DATA);

        String type = getIntent().getStringExtra(BerryConstant.AD_TYPE);

        switch (type) {
            case BerryConstant.AD_TYPE_GENERIC:
                htmlData = dbHelper.getYourData(BerryConstant.AD_TYPE_GENERIC);
                break;
            case BerryConstant.AD_TYPE_GEOFENCE:
                htmlData = dbHelper.getYourData(BerryConstant.AD_TYPE_GEOFENCE);
                break;
            case BerryConstant.AD_TYPE_GEOFENCE_NOTIFICATION:
                htmlData = dbHelper.getYourData(BerryConstant.AD_TYPE_GEOFENCE);
                break;
        }

        startWebView(htmlData);
    }

    private void startWebView(String htmlData) {
        myImageView = (ImageView) findViewById(R.id.image);
        mywebView = (WebView) findViewById(R.id.webview);
        WebSettings myWebSettings = mywebView.getSettings();
        myWebSettings.setJavaScriptEnabled(true);
        mywebView.loadDataWithBaseURL("", htmlData, "text/html", "UTF-8", "");
        mywebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        myImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }


}