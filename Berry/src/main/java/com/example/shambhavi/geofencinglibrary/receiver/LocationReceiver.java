package com.example.shambhavi.geofencinglibrary.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.widget.Toast;

import java.security.Provider;

/**
 * Created by akash on 31/5/16.
 */
public class LocationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                Toast.makeText(context, "GPS Disabled ", Toast.LENGTH_SHORT).show();
            }else{
              Toast.makeText(context, "GPS Enabled ", Toast.LENGTH_SHORT).show();
             /*     Intent pushIntent = new Intent(context, Provider.Service.class);
                context.startService(pushIntent);
*/
                Intent intentp = new Intent();
                intent.setAction("com.mobmery.ads.create.geofence");
                context.sendBroadcast(intentp);
            }

        }
    }
}
