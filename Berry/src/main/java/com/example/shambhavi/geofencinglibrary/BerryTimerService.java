package com.example.shambhavi.geofencinglibrary;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;
import com.example.shambhavi.geofencinglibrary.util.BerryUtils;
import com.example.shambhavi.geofencinglibrary.util.DownloadWebPageTask;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by akash on 2/6/16.
 */
public class BerryTimerService extends Service {
    final Handler handler = new Handler();
    Timer timer;
    TimerTask timerTask;
    Context mContext;
    String DEBUG_TAG = "Berry_Timer_Service";
    Context context;
    boolean isGeofence = false;
    String geofenceid="trst";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = BerryTimerService.this;
        startTimer();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getStringExtra("isGeofence") != null) {
            if (intent.getStringExtra("isGeofence").equalsIgnoreCase("yes")) {
                isGeofence = true;
            }
        }
        if(intent.getStringExtra("geofenceid")!=null){
            geofenceid=intent.getStringExtra("geofenceid");
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimertask();
    }


    public void startTimer() {
        timer = new Timer();
        initializeTimerTask();
        timer.schedule(timerTask, 10 * 1000, 10 * 1000);
    }


    public void stopTimertask() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(mContext, "Starting service....", Toast.LENGTH_LONG).show();
                        ConnectivityManager connMgr = (ConnectivityManager)
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                        if (networkInfo != null && networkInfo.isConnected()) {


                            //new DownloadWebpageTask().execute(MobmerryAd.getInstance(mContext).getmGeofenceAdsUrl());
                            if (isGeofence) {
                                DownloadWebPageTask genericTask = new DownloadWebPageTask(mContext, BerryConstant.AD_TYPE_GEOFENCE);
                                genericTask.execute(MobmerryAd.getInstance(mContext).getmGeofenceAdsUrl()+"/"+geofenceid);
                            } else {
                                DownloadWebPageTask genericTask = new DownloadWebPageTask(mContext, BerryConstant.AD_TYPE_GENERIC);
                                genericTask.execute(MobmerryAd.getInstance(mContext).getmGenericAdsUrl());
                            }


                        }
                    }
                });
            }
        };
    }

    public String convertToHtml(String result) {

        String path = Environment.getExternalStorageDirectory().getPath();
        String fileName;

        if (isGeofence)
            fileName = "geofence" + ".html";
        else
            fileName = "generic" + ".html";

        try {
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + fileName);
            file.setReadable(true, false);
            file.createNewFile();
            if (file.exists()) {
                OutputStream fo = new FileOutputStream(file);
                fo.write(result.getBytes());
                fo.close();
            }
            path = file.getAbsolutePath();
            Log.d(DEBUG_TAG, path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "FIle not found exception", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "IO EXCeption", Toast.LENGTH_SHORT).show();
        }
        return path;
    }

    /**
     * Its deprecated use Class DownloadWebPageTask (P capital)
     */
    @Deprecated
    class DownloadWebpageTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {

                return BerryUtils.downloadHTMLFromUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(getBaseContext());
            String htmlData = mDbHelper.insertingDataIntoDatabase(result, BerryConstant.AD_TYPE_GENERIC);
            Intent i = new Intent(mContext, LibActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra(BerryConstant.AD_TYPE, BerryConstant.AD_TYPE_GENERIC);
            i.putExtra(BerryConstant.HTML_DATA, htmlData);
            startActivity(i);
        }
    }
}




