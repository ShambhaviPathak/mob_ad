package com.example.shambhavi.geofencinglibrary.util;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.shambhavi.geofencinglibrary.FeedReaderDbHelper;
import com.example.shambhavi.geofencinglibrary.LibActivity;
import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;

import java.io.IOException;

/**
 * Created by akash on 16/6/16.
 */
public class DownloadWebPageTask extends AsyncTask<String, Void, String> {

    private Context mContext;
    private String type;

    public DownloadWebPageTask(Context context, String type) {
        this.mContext = context;
        this.type = type;
    }

    @Override
    protected String doInBackground(String... urls) {
        try {

            return BerryUtils.downloadHTMLFromUrl(urls[0]);
        } catch (IOException e) {
            return "Unable to retrieve web page. URL may be invalid.";
        }
    }

    @Override
    protected void onPostExecute(String result) {


        FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(mContext);
        // String htmlData = mDbHelper.insertingDataIntoDatabase(result, BerryConstant.AD_TYPE_GENERIC);


        Intent i = new Intent(mContext, LibActivity.class);

        String htmlData = "";
        switch (type) {
            case BerryConstant.AD_TYPE_GENERIC:
                htmlData = mDbHelper.insertingDataIntoDatabase(result, BerryConstant.AD_TYPE_GENERIC);
                i.putExtra(BerryConstant.AD_TYPE, BerryConstant.AD_TYPE_GENERIC);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(BerryConstant.HTML_DATA, htmlData);
                mContext.startActivity(i);
                break;
            case BerryConstant.AD_TYPE_GEOFENCE_NOTIFICATION:
                //For Geofence notification type just download the data from the Geofence url and save it in sqlite. When
                //user clicks on the notification it will open the saved data
                htmlData = mDbHelper.insertingDataIntoDatabase(result, BerryConstant.AD_TYPE_GEOFENCE);
                break;
            case BerryConstant.AD_TYPE_GEOFENCE:
                //For just Geofence type download the data and call libactivity and pass the html data.
                htmlData = mDbHelper.insertingDataIntoDatabase(result, BerryConstant.AD_TYPE_GEOFENCE);
                i.putExtra(BerryConstant.AD_TYPE, BerryConstant.AD_TYPE_GEOFENCE);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(BerryConstant.HTML_DATA, htmlData);
                mContext.startActivity(i);
                break;
        }


    }
}
