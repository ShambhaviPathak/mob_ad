package com.example.shambhavi.geofencinglibrary.controller;

/**
 * Created by skytreasure on 30/12/15.<br/>
 *
 * This is the baseinterface, this should be implemented in all the Activities where network calls are made
 *
 */
public interface BaseInterface {

    /**
     *
     * @param object
     * @param requestCode
     */
    void handleNetworkCall(Object object, int requestCode);

}
