package com.example.shambhavi.geofencinglibrary.constants;

import com.example.shambhavi.geofencinglibrary.model.BerryAdModel;

import java.util.List;

/**
 * Created by akash on 31/5/16.
 */
public class BerryConstant {

    /**
     * Url *
     */
  /* public static final String BASE_URL = "https://sky-firebase.firebaseapp.com/";
    public static final String CONFIG_URL = BASE_URL + "config.html";
    public static final String LOCATION_URL = BASE_URL + "location.html";*/

    public static String BASE_URL="http://192.168.1.101:8000/ad/";
    public static final String CONFIG_URL = BASE_URL + "config";
    public static final String LOCATION_URL = BASE_URL + "location";



    /**
     * REQUEST TYPE *
     */
    public static final int CONFIG_REQUEST=1;
    public static final int LOCATION_REQEST=2;

    /**
     * Broadcast Intents
     */
    public static final String GET_GEOFENCE_DATA_FROM_SERVER="com.mobmery.ads.get.geofence";
    public static final String CREATE_GEOFENCE_FOR_ADS="com.mobmery.ads.create.geofence";

    /**
     * NETWORK REQUEST JSON OBJECT *
     */
    public static final String GEOFENCES = "geofences";
    public static final String ADS_URL = "adurls";
    public static final String AD_TYPE="ads_type";
    public static final String HTML_DATA="html_data";
    public static final String AD_TYPE_GEOFENCE_NOTIFICATION="ads_type_geofence_notification";
    public static final String AD_TYPE_GEOFENCE="ads_type_geofence";
    public static final String AD_TYPE_GENERIC="ads_type_generic";
     public static final String AD_TYPE_PROXIMITY="ads_type_proximity";
    /**
     * Data to store geofence data.
     */
    public static List<BerryAdModel> mBerryAds;
}


