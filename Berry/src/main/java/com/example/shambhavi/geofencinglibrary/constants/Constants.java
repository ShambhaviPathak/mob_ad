package com.example.shambhavi.geofencinglibrary.constants;

import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

/**
 * Created by shambhavi on 27/5/16.
 */
public class Constants {
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = 60*60*1000;
    public static final float GEOFENCE_RADIUS_IN_METERS = 2000;
    public static final String GEOFENCE_CONFIG_URL="https://sky-firebase.firebaseapp.com/";
    public static final HashMap<String, LatLng> LANDMARKS = new HashMap<String, LatLng>();
    static {
        LANDMARKS.put("Kasturi Nagar", new LatLng(13.003903,77.660330));
    }
}
