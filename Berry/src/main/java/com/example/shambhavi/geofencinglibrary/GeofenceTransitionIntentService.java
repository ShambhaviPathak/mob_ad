package com.example.shambhavi.geofencinglibrary;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;

import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;
import com.example.shambhavi.geofencinglibrary.util.BerryUtils;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by shambhavi on 27/5/16.
 */
public class GeofenceTransitionIntentService extends IntentService {

    protected static final String TAG = "GeofenceTransitionsIS";

    public GeofenceTransitionIntentService()
    {
        super(TAG);  // use TAG to name the IntentService worker thread
    }

    private static String getGeofenceTransitionDetails(GeofencingEvent event) {
        String transitionString =
                GeofenceStatusCodes.getStatusCodeString(event.getGeofenceTransition());
        List triggeringIDs = new ArrayList();
        for (Geofence geofence : event.getTriggeringGeofences()) {
            triggeringIDs.add(geofence.getRequestId());
        }
        return String.format("%s: %s", transitionString, TextUtils.join(", ", triggeringIDs));
    }

    public static boolean checkApp(Context context) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(ACTIVITY_SERVICE);

        // get the info from the currently running task
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);

        ComponentName componentInfo = taskInfo.get(0).topActivity;
        return componentInfo.getPackageName().equalsIgnoreCase("com.example.shambhavi.geofencinglibrary");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent event = GeofencingEvent.fromIntent(intent);
        if (event.hasError()) {
            Log.e(TAG, "GeofencingEvent Error: " + event.getErrorCode());

            return;
        } else {
            // Get the transition type.
            int geofenceTransition = event.getGeofenceTransition();
            // String description = getGeofenceTransitionDetails(event);

            if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||geofenceTransition==Geofence.GEOFENCE_TRANSITION_DWELL ||
                    geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ) {

                List<Geofence> triggeringGeofences = event.getTriggeringGeofences();

                String geofenceTransitionDetails = getGeofenceTransitionDetails(
                        this,
                        geofenceTransition,
                        triggeringGeofences
                );

                if (BerryUtils.isAppIsInBackground(this)) {
                    try {
                        String htmldata = BerryUtils.downloadHTMLFromUrl(MobmerryAd.getInstance(this).getmGeofenceAdsUrl()+ "/"+geofenceTransitionDetails);
                        FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(getBaseContext());
                        String htmlData = mDbHelper.insertingDataIntoDatabase(htmldata, BerryConstant.AD_TYPE_GEOFENCE);
                        displayNotification(geofenceTransitionDetails, htmlData);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    if (BerryUtils.isMyServiceRunning(BerryTimerService.class, this)) {
                        MobmerryAd.stopBerrryService(this);
                    }
                    Intent intent1 = new Intent(this, BerryTimerService.class);
                    intent1.putExtra("isGeofence", "yes");
                    intent1.putExtra("geofenceid",geofenceTransitionDetails);
                    startService(intent1);
                }

            }

        }
    }

    private String getGeofenceTransitionDetails(
            Context context,
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList triggeringGeofencesIdsList = new ArrayList();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ", triggeringGeofencesIdsList);

        //return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
        return  triggeringGeofencesIdsString;
    }

    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }

    public void displayNotification(String msg, String htmlData) {
        Intent notificationIntent = new Intent(getApplicationContext(), LibActivity.class);
        notificationIntent.putExtra(BerryConstant.AD_TYPE, BerryConstant.AD_TYPE_GEOFENCE);
        // FeedReaderDbHelper dbHelper=new FeedReaderDbHelper(this);
        //  String htmlData = dbHelper.getYourData(BerryConstant.AD_TYPE_GEOFENCE);
        notificationIntent.putExtra(BerryConstant.HTML_DATA, htmlData);
        // Construct a task stack.
        android.support.v4.app.TaskStackBuilder stackBuilder = android.support.v4.app.TaskStackBuilder.create(this);

        // Add the main Activity to the task stack as the parent.
        stackBuilder.addParentStack(LibActivity.class);

        // Push the content Intent onto the stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Get a PendingIntent containing the entire back stack.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        int notif_id = (int) (System.currentTimeMillis() & 0xFFL);
        android.support.v4.app.NotificationCompat.Builder builder = new android.support.v4.app.NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.common_full_open_on_phone)
                // In a real app, you may want to use a library like Volley
                // to decode the Bitmap.
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.mobmerry))
                .setColor(Color.RED)
                .setContentTitle("Berry Ads")
                .setContentText("Welcome to Mobmerry Region")
                .setContentIntent(notificationPendingIntent);
        builder.setAutoCancel(true);
        builder.setOngoing(false);

        // Get an instance of the Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Issue the notification
        Random random=new Random();

        mNotificationManager.notify(/*notif_id*/1111, builder.build());
    }
}
