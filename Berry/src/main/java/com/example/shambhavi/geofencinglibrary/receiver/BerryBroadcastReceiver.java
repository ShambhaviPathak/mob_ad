package com.example.shambhavi.geofencinglibrary.receiver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.shambhavi.geofencinglibrary.GeofenceTransitionIntentService;
import com.example.shambhavi.geofencinglibrary.R;
import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;
import com.example.shambhavi.geofencinglibrary.controller.BaseInterface;
import com.example.shambhavi.geofencinglibrary.model.BerryAdModel;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class BerryBroadcastReceiver extends BroadcastReceiver implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<Status> {

    protected static final String TAG = "BroadcastReceiver";
    protected ArrayList<Geofence> myGeofenceList;
    protected GoogleApiClient mGoogleApiClient;
    Context context;
    List<BerryAdModel> geogenceList;
    boolean createGeoFenceFlag = false;
    boolean getDataFlag = false;
    private PendingIntent mGeofencePendingIntent;
    private BerryAdModel mMobmerryConfigModel, mMobmerryLocationModel;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        if (!(intent.getAction() == null)) {

            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {

                final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    createGeoFenceFlag = true;
                }
            } else {
                switch (intent.getAction()) {
                    case BerryConstant.CREATE_GEOFENCE_FOR_ADS:
                        createGeoFenceFlag = true;
                        break;
                    case BerryConstant.GET_GEOFENCE_DATA_FROM_SERVER:
                        getDataFlag = true;
                        break;
                }

            }

            myGeofenceList = new ArrayList<Geofence>();
            mGeofencePendingIntent = null;
            Gson gson = new Gson();
            buildGoogleApiClient();

            mMobmerryLocationModel = new BerryAdModel(context, new BaseInterface() {
                @Override
                public void handleNetworkCall(Object object, int requestCode) {
                    try {
                        if (requestCode == BerryConstant.LOCATION_REQEST) {
                            if (object instanceof ArrayList) {
                                geogenceList = (ArrayList<BerryAdModel>) object;

                                BerryConstant.mBerryAds = geogenceList;

                                if (createGeoFenceFlag) {
                                    populateGeofenceList(BerryConstant.mBerryAds);
                                    addGeofences();

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            mMobmerryConfigModel = new BerryAdModel(context, new BaseInterface() {
                @Override
                public void handleNetworkCall(Object object, int requestCode) {
                    try {
                        if (requestCode == BerryConstant.CONFIG_REQUEST) {
                            if (object instanceof Boolean) {
                                //Config load is successful.
                                if (createGeoFenceFlag) {
                                    mMobmerryLocationModel.getGeofences();
                                }
                            } else {
                                //CONFIG LOAD ERROR
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            });


        }


    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void populateGeofenceList(List<BerryAdModel> geofenceList) {
        try {

            for (BerryAdModel fence : geofenceList) {
                myGeofenceList.add(new Geofence.Builder()
                        // Set the request ID of the geofence. This is a string to identify this geofence.
                        .setRequestId(fence.getmId())

                        // Set the circular region of this geofence.
                        .setCircularRegion(
                                Double.parseDouble(fence.getmLatitude()),
                                Double.parseDouble(fence.getmLongitude()),
                                Float.parseFloat(fence.getmRange()) * 1000
                        )


                        // Set the expiration duration of the geofence. This geofence gets automatically
                        // removed after this period of time.
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)

                        // Set the transition types of interest. Alerts are only generated for these
                        // transition. We track entry and exit transitions in this sample.
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_ENTER |
                                Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setLoiteringDelay(1000)
                        .setNotificationResponsiveness(5000)

                        // Create the geofence.
                        .build());
            }
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT);
        }

    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(context, GeofenceTransitionIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        return PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);


        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(myGeofenceList);

        // Return a GeofencingRequest.
        return builder.build();
    }

    public void addGeofences() {
        if (!mGoogleApiClient.isConnected()) {
            Toast.makeText(context, context.getString(R.string.not_connected), Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            LocationServices.GeofencingApi.addGeofences(
                    mGoogleApiClient,
                    // The GeofenceRequest object.
                    getGeofencingRequest(),
                    // A pending intent that that is reused when calling removeGeofences(). This
                    // pending intent is used to generate an intent when a matched geofence
                    // transition is observed.
                    getGeofencePendingIntent()
            ).setResultCallback(this); // Result processed in onResult().
        } catch (SecurityException securityException) {
            // Catch exception generated if the app does not use ACCESS_FINE_LOCATION permission.
            logSecurityException(securityException);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logSecurityException(SecurityException securityException) {
        Log.e(TAG, "Invalid location permission. " +
                "You need to use ACCESS_FINE_LOCATION with geofences", securityException);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (createGeoFenceFlag || getDataFlag) {
            mMobmerryConfigModel.getConfig();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull Status status) {

    }
}
