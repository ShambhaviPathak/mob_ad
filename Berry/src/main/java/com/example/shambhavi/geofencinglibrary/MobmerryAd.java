package com.example.shambhavi.geofencinglibrary;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import com.example.shambhavi.geofencinglibrary.constants.BerryConstant;
import com.example.shambhavi.geofencinglibrary.util.BerryUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by akash on 31/5/16.
 */
public class MobmerryAd {

    static Intent intent;
    private static MobmerryAd mInstance = null;
    final Handler handler = new Handler();
    Timer timer;
    TimerTask timerTask;
    private String mGeofenceAdsUrl;
    private String mGenericAdsUrl;
    private String mBluetoothUrl;
    private Context mContext;


    private MobmerryAd(Context context,boolean gpsIsOn){
        mContext=context;
        Intent i=new Intent();
        if(!gpsIsOn){

            //send Broadcast , get the data from server but don't create geofence.
            i.setAction(BerryConstant.GET_GEOFENCE_DATA_FROM_SERVER);
        }else{
            //Send Broadcast, get the data from server and create geofence.
            i.setAction(BerryConstant.CREATE_GEOFENCE_FOR_ADS);

        }
        context.sendBroadcast(i);

    }


    public static MobmerryAd getInstance(Context context){
        intent = new Intent(context, BerryTimerService.class);

        if(BerryUtils.isMarshmallow(context)){
            if(BerryUtils.isLocationPermissionGiven(context)){

                mInstance=locationCheck(context);

            }else{
                //If location permission is not given, just get the data from server and start the service.
                if(mInstance == null)
                {
                    mInstance = new MobmerryAd(context,false);
                    return mInstance;
                }
                startBerryTimerService(context);
            }
        }else{
            mInstance=locationCheck(context);
        }

        return mInstance;
    }

    public static void setConfigURL(String url){
        BerryConstant.BASE_URL=url;
    }

    private static MobmerryAd locationCheck(Context context){
        if(BerryUtils.isGpsON(context)){
            //If gps is on create geofence
            if(mInstance == null)
            {
                mInstance = new MobmerryAd(context,true);

                stopBerrryService(context);
                return mInstance;
            }
        }else{
            //If GPS is OFF, just get the data from server
            if(mInstance == null)
            {
                mInstance = new MobmerryAd(context,false);
                return mInstance;
            }
            startBerryTimerService(context);
        }

        return mInstance;
    }


    public static void startBerryTimerService(Context context){
        if (isMyServiceRunning(BerryTimerService.class, context)) {
            stopBerrryService(context);
        }
        if(!isMyServiceRunning(BerryTimerService.class,context)){
            if (BerryUtils.isMarshmallow(context)) {
                if (!BerryUtils.isLocationPermissionGiven(context)) {
                    intent = new Intent(context, BerryTimerService.class);
                    context.startService(intent);
                } else {
                    if (!BerryUtils.isGpsON(context)) {
                        intent = new Intent(context, BerryTimerService.class);
                        context.startService(intent);
                    }
                }
            } else {
                if (!BerryUtils.isGpsON(context)) {
                    intent = new Intent(context, BerryTimerService.class);
                    context.startService(intent);
                }
            }
        }
    }


    private static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void stopBerrryService(Context context){
        context.stopService(intent);
    }

    public String getmGeofenceAdsUrl() {
        return mGeofenceAdsUrl;
    }

    public void setmGeofenceAdsUrl(String mGeofenceAdsUrl) {
        this.mGeofenceAdsUrl = mGeofenceAdsUrl;
    }

    public String getmGenericAdsUrl() {
        return mGenericAdsUrl;
    }

    public void setmGenericAdsUrl(String mGenericAdsUrl) {
        this.mGenericAdsUrl = mGenericAdsUrl;
    }

    public String getmBluetoothUrl() {
        return mBluetoothUrl;
    }

    public void setmBluetoothUrl(String mProximityAdsUrl) {
        this.mBluetoothUrl = mProximityAdsUrl;
    }


}
